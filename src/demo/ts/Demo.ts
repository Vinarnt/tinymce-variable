import { TinyMCE } from 'tinymce';

import Plugin from '../../main/ts/Plugin';

declare let tinymce: TinyMCE;

Plugin();

tinymce.init({
  // basic tinyMCE stuff
  selector: "textarea",
  content_css: 'style.css',
  menubar: false,
  toolbar: "bold italic | variable code help",
  statusbar: false,
  // variable plugin related
  plugins: "variable code help",
  variable_mapper: {
    username: 'Username',
    phone: 'Phone',
    community_name: 'Community name',
    email: 'Email address',
    sender: 'Sender name',
    account_id: 'Account ID'
  },
  variable_menu_items: [
    'account_id', 'community_name', 'username', {
      text: 'Contact',
      items: [
        'phone', 'email'
      ]
    }, 'sender'
  ],
  language: 'en_US',
  language_url: '../../../scratch/compiled/langs/en_US.js'
  // variable_prefix: '{{',
  // variable_suffix: '}}'
  // variable_class: 'bbbx-my-variable',
  //variable_valid: ['username', 'sender', 'phone', 'community_name', 'email']
});
