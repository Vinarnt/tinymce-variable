import { Editor, TinyMCE } from 'tinymce';

declare const tinymce: TinyMCE;

const setup = (editor: Editor): any => {
  /**
     * Object that is used to replace the variable string to be used
     * in the HTML view
     * @type {object}
     */
  const mapper = editor.getParam("variable_mapper", {});

  /**
   * Array defining the menu items order and grouping
   * @type {array}
   */
  const menuItems = editor.getParam("variable_menu_items", null);

  /**
   * define a list of variables that are allowed
   * if the variable is not in the list it will not be automatically converterd
   * by default no validation is done
   * @todo  make it possible to pass in a function to be used a callback for validation
   * @type {array}
   */
  const valid = editor.getParam("variable_valid", null);

  /**
   * Get custom variable class name
   * @type {string}
   */
  const className = editor.getParam("variable_class", "variable");

  /**
   * Prefix and suffix to use to mark a variable
   * @type {string}
   */
  const prefix = editor.getParam("variable_prefix", "{{");
  const suffix = editor.getParam("variable_suffix", "}}");

  /**
   * RegExp is not stateless with '\g' so we return a new variable each call
   * @return {RegExp}
   */
  function getStringVariableRegex() {
    return new RegExp(
      prefix + "([a-zA-Z0-9._ \\u00C0-\\u017F]*)?" + suffix,
      "g"
    );
  }

  /**
   * check if a certain variable is valid
   * @param {string} name
   * @return {bool}
   */
  function isValid(name) {
    if (!valid || valid.length === 0) return true;

    const validString = "|" + valid.join("|") + "|";

    return validString.indexOf("|" + name + "|") > -1 ? true : false;
  }

  function getMappedValue(cleanValue) {
    if (typeof mapper === "function") return mapper(cleanValue);

    return Object.prototype.hasOwnProperty.call(mapper, cleanValue)
      ? mapper[cleanValue]
      : cleanValue;
  }

  /**
   * Strip variable to keep the plain variable string
   * @example "{test}" => "test"
   * @param {string} value
   * @return {string}
   */
  function cleanVariable(value) {
    return value.replace(/[^a-zA-Z0-9._\s\u00C0-\u017F]/g, "");
  }

  /**
   * convert a text variable "x" to a span with the needed
   * attributes to style it with CSS
   * @param  {string} value
   * @return {string}
   */
  function createHTMLVariable(value) {
    const cleanValue = cleanVariable(value);

    // check if variable is valid
    if (!isValid(cleanValue)) return value;

    const cleanMappedValue = getMappedValue(cleanValue);

    editor.fire("variableToHTML", {
      value: value,
      cleanValue: cleanValue,
    });

    const variable = prefix + cleanValue + suffix;
    return (
      '<span class="' +
      className +
      '" data-original-variable="' +
      variable +
      '" contenteditable="false">' +
      cleanMappedValue +
      "</span>"
    );
  }

  /**
   * convert variable strings into html elements
   * @return {void}
   */
  function stringToHTML() {
    const nodeList = [];
    let nodeValue,
      node,
      div;

    // find nodes that contain a string variable
    tinymce.walk(
      editor.getBody(),
      function (n) {
        if (
          n.nodeType == 3 &&
          n.nodeValue &&
          getStringVariableRegex().test(n.nodeValue)
        ) {
          nodeList.push(n);
        }
      },
      "childNodes"
    );

    // loop over all nodes that contain a string variable
    for (let i = 0; i < nodeList.length; i++) {
      nodeValue = nodeList[i].nodeValue.replace(
        getStringVariableRegex(),
        createHTMLVariable
      );
      div = editor.dom.create("div", null, nodeValue);
      while ((node = div.lastChild)) {
        editor.dom.insertAfter(node, nodeList[i]);
      }

      editor.dom.remove(nodeList[i]);
    }
  }

  /**
   * convert HTML variables back into their original string format
   * for example when a user opens source view
   * @return {void}
   */
  function htmlToString() {
    const nodeList = [];
    let nodeValue,
      node,
      div;

    // find nodes that contain a HTML variable
    tinymce.walk(
      editor.getBody(),
      function (n) {
        if (n.nodeType == 1) {
          const original = n.getAttribute("data-original-variable");
          if (original !== null) {
            nodeList.push(n);
          }
        }
      },
      "childNodes"
    );

    // loop over all nodes that contain a HTML variable
    for (let i = 0; i < nodeList.length; i++) {
      nodeValue = nodeList[i].getAttribute("data-original-variable");
      div = editor.dom.create("div", null, nodeValue);
      while ((node = div.lastChild)) {
        editor.dom.insertAfter(node, nodeList[i]);
      }

      // remove HTML variable node
      // because we now have an text representation of the variable
      editor.dom.remove(nodeList[i]);
    }
  }

  /**
   * handle formatting the content of the editor based on
   * the current format. For example if a user switches to source view and back
   * @param  {object} e
   * @return {void}
   */
  function handleContentRerender(e) {
    return e.format === "raw" ? stringToHTML() : htmlToString();
  }

  /**
   * insert a variable into the editor at the current cursor location
   * @param {string} value
   * @return {void}
   */
  function addVariable(value) {
    const htmlVariable = createHTMLVariable(value);
    editor.selection.setContent(htmlVariable);
  }

  function isVariable(element) {
    if (
      typeof element.getAttribute === "function" &&
      element.hasAttribute("data-original-variable")
    )
      return true;

    return false;
  }

  /**
   * Trigger special event when user clicks on a variable
   * @return {void}
   */
  function handleClick(e) {
    const target = e.target;

    if (!isVariable(target)) return null;

    // put the cursor right after the variable
    if (e.target.nextSibling) {
      editor.selection.setCursorLocation(e.target.nextSibling, 0);
    } else {
      editor.selection.select(e.target);
      editor.selection.collapse();
    }

    // and trigger event if we want to do something special
    const value = target.getAttribute("data-original-variable");
    editor.fire("variableClick", {
      value: cleanVariable(value),
      target: target,
    });
  }

  function preventDrag(e) {
    const target = e.target;

    if (!isVariable(target)) return null;

    e.preventDefault();
    e.stopImmediatePropagation();
  }

  let currentDirection;

  function keyDown(e) {
    if (e.keyCode == 37) {
      currentDirection = "left";
    } else if (e.keyCode == 39) {
      currentDirection = "right";
    } else if (e.keyCode == 38) {
      currentDirection = "up";
    } else if (e.keyCode == 40) {
      currentDirection = "down";
    } else if (e.keyCode == 8) {
      const r = editor.selection.getRng();
      if (
        r.collapsed &&
        r.startOffset >= 1 &&
        r.startContainer.textContent.charCodeAt(r.startOffset) == 65279
      ) {
        // work around TinyMCE failing to delete a character when it's got some zero-width non-blocking char
        r.startContainer.textContent =
          r.startContainer.textContent.slice(0, r.startOffset - 1) +
          r.startContainer.textContent.slice(r.startOffset + 1);

        e.preventDefault();
        e.stopImmediatePropagation();
      }
    }
  }

  function nodeChange(e) {
    const target = e.element;

    if (!isVariable(target)) return null;

    e.preventDefault();
    e.stopImmediatePropagation();

    switch (currentDirection) {
      case "left":
      case "up":
        editor.selection.select(target);
        editor.selection.collapse(true);
        break;

      case "down":
      case "right":
        editor.selection.select(target);
        editor.selection.collapse();
        break;
    }
  }

  editor.on("beforegetcontent", handleContentRerender);
  editor.on("init", stringToHTML);
  editor.on("getcontent", stringToHTML);
  editor.on("click", handleClick);
  editor.on("mousedown", preventDrag);
  editor.on("keydown", keyDown);
  editor.on("NodeChange", nodeChange);
  editor.ui.registry.addMenuButton("variable", {
    text: editor.translate("variable.button"),
    fetch: (callback) => {
      let items;
      if (menuItems) {
        // Order and group menu items
        items = menuItems.map((item) => {
          if (typeof item === "object") {
            // sub menu
            return editor.ui.registry.addNestedMenuItem(
              "nesteditem",
              {
                text: editor.translate(item.text),
                getSubmenuItems: function () {
                  return item.items.map((value) => ({
                    type: "menuitem",
                    text: editor.translate(mapper[value]),
                    onAction: function () {
                      addVariable(value);
                    },
                  }));
                },
              }
            );
          } else {
            return {
              type: "menuitem",
              text: mapper[item],
              onAction: function () {
                addVariable(item);
              },
            };
          }
        });
      } else {
        items = Object.entries(mapper).map(([key, value]) => ({
          type: "menuitem",
          text: editor.translate(value),
          onAction: function () {
            addVariable(key);
          },
        }));
      }
      callback(items);
    },
  });

  return {
    getMetadata: function () {
      return {
        name: "Variable plugin",
        url: "https://gitlab.com/Vinarnt/tinymce-variable",
      };
    },
  };
};

export default (): void => {
  tinymce.PluginManager.requireLangPack('variable', 'en,fr_FR');
  tinymce.PluginManager.add('variable', setup);
};
